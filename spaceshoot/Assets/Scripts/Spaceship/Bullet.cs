using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private Rigidbody2D rb;
        
        public void Init(Vector2 direction)
        {
            Move(direction);
        }

        private void Awake()
        {
            Debug.Assert(rb != null, "rigidbody2D cannot be null");
            DontDestroyOnLoad(this);
        }

        private void Move(Vector2 direction)
        {
            rb.velocity = direction * speed;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("ObjectDestroyer"))
            {
                //Destroy(this.gameObject);
                gameObject.SetActive(false);
                return;
            }

            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
        }
    }
}