﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class UIManager1 : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI finalScoreText;
        [SerializeField] private Button restartButton;
        [SerializeField] private RectTransform endDialog;
    
        private void Awake()
        {
            Debug.Assert(restartButton != null, "restartButton cannot be null");
            Debug.Assert(endDialog != null, "endDialog cannot be null");   
        
            restartButton.onClick.AddListener(OnRestartButtonClicked);
        }

        private void Start()
        {
            GameManager.Instance.OnRestarted += RestartUI;
            ScoreManager.Instance.OnScoreUpdated += UpdateScoreUI;
            ShowEndDialog(false);
            ShowScore(false);
            UpdateScoreUI();
        }

        public void OnRestartButtonClicked()
        {
            ShowEndDialog(false);
            UpdateScoreUI();
            ShowScore(true);
            SceneManager.LoadScene("Game");
        } 
    
        private void UpdateScoreUI()
        {
            scoreText.text = $"Score : {ScoreManager.Instance.GetScore()}";
            finalScoreText.text = $"Player Score : {ScoreManager.Instance.GetScore()}";
        }

        private void RestartUI()
        {
            ShowScore(false);
            ShowEndDialog(true);
        }
    
        public void ShowScore(bool showScore)
        {
            //UpdateScoreUI();
            scoreText.gameObject.SetActive(showScore);
        }

        private void ShowEndDialog(bool showDialog)
        {
            //UpdateScoreUI();
            endDialog.gameObject.SetActive(showDialog);
        }

        private void OnDestroy()
        {
            GameManager.Instance.OnRestarted -= RestartUI;
            ScoreManager.Instance.OnScoreUpdated -= UpdateScoreUI;            
        }
    }
}
